package com.example.foodorderingapp;

import androidx.annotation.NonNull;

public class Dish {

    //Constructor
    public Dish(@NonNull String name, @NonNull String desc, @NonNull Integer priceInCents, @NonNull String imageName){

        this.name = name;
        this.desc = desc;
        this.priceInCents= priceInCents;
        this.imageName = imageName;

    }

    public String name;
    public String desc;
    public Integer priceInCents;
    public String imageName;

    //Public methods

    @Override
    public String toString(){
        return "Dish{"+
                "name = "+name+
                ", description = "+desc+
                ", price in cents = "+priceInCents+
                ", image name = "+ imageName +'}';
    }

    public Boolean equals(Dish dish){
        return (name.equals(dish.name) &&
                desc.equals(dish.desc) &&
                dish.priceInCents == priceInCents &&
                imageName.equals(dish.imageName));
    }
    
}
