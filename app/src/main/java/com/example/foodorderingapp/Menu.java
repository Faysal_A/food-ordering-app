package com.example.foodorderingapp;

import java.util.ArrayList;
import java.util.HashMap;

public class Menu {

    //Constructor
    public Menu() {
        populateMenu();
    }

    //Public method
    public HashMap<String, ArrayList<Dish>> dishesByCategory() {
        if(menu == null){
            populateMenu();
        }
        return menu;
    }

    //Private properties
    private HashMap<String, ArrayList<Dish>> menu = null; // Category : Dish

    private void populateMenu() {
        menu = new HashMap<>();

        //Go through all the categories
        for (Cuisine cuisine : Cuisine.values()) {
            ArrayList<Dish> dishesList = new ArrayList<>();

            for(int i = 0; i < 6; i++){
                Dish dish = new Dish("1", "1 desc", 199, "");
                dishesList.add(dish);
            }

            //Add dishesList to menu dictionary
            menu.put(cuisine.toString(), dishesList);
        }

        System.out.println("Menu = "+ menu);
    }
}